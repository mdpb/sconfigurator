import { spaceErr, protocolCheck } from "../src/linting/csv-linter.js";
import { assert } from "chai";
describe('SpaceErr', function () {
    it('should catch an illegal space character', function() {
        assert.equal(spaceErr('check, this,fails'), true)
    }) 
})

describe('ProtoCheck', function () {
    it('should catch anything with http in it', function () {
        assert.equal(protocolCheck('http://mysite.com'), true)
        assert.equal(protocolCheck('*.someRandomURI.com,https://google.com'), true)
        assert.equal(protocolCheck('*.someRandomURI.com'), false)
    })
})