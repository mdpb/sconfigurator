export function spaceErr(csvString) {
    csvString.trim()
    if (csvString.includes(', ')) {
        return true
    }
    return false
}

export function protocolCheck(csvString) {
    csvString.trim()
    if (csvString.includes('http')) {
        return true
    }
    return false
}